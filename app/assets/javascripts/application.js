// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery
//= require_tree .

var updateDescriptions;

updateDescriptions = function () {
    var selection_id;
    selection_id = $('#employee_role_id').val();
    return $.getJSON('/roles/' + selection_id + '/descriptions', {}, function (json, response){
        //console.log(json['description']);
        return $('#descriptions').text(json['description']);
    });
};

$(document).on('turbolinks:load', function () {
    console.log("role loading");
    if ($('#employee_role_id').length){
        updateDescriptions();
    }
    $('#employee_role_id').on('change', updateDescriptions);
});

