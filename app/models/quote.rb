class Quote < ApplicationRecord
  belongs_to :employee
  belongs_to :car
  
    def vehicle_markup
    self.total_amount = (car.wholesale_price * 0.082)
  end

  def sales_tax
    self.total_amount = (vehicle_markup + car.wholesale_price) * 0.043
  end

  def total_cost
    self.total_amount = (car.wholesale_price + vehicle_markup + sales_tax)
    end
end
