class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
		 
		
		def salesman?
			role == "salesman"
		end
		
		def salesmanager?
			role == "salesmanager"
		end
		
		def financemanager?
			role == "financemanager"
		end
		
		def inventorymanager?
			role == "inventorymanager"
		end
		
		def dealershipowner?
			role == "dealershipowner"
		end
					
end
