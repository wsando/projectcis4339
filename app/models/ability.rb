class Ability
  include CanCan::Ability

  def initialize(user)
	user ||= User.new(role: "dealershipowner")
	
	if user.dealershipowner?
	
	can :manage, Car
	can :manage, Customer
	can :manage, Employee
	can :manage, Loan
	can :manage, Quote
	can :manage, Role
	
# *NOTE* manage = God mode. Create, Read, Update, Delete. Grants full-access for all tables. 
# *NOTE* read = read-only access for all tables
# *NOTE* if an attribute is missing, the person should not have access to it. 

	elsif user.salesman?
	
	can :manage, Car
	can :manage, Customer
#	can :read, Employee ---> salesman does not need even read-access to Employee table
	can :read, Loan
	can :manage, Quote
#	can :read, Role ---> salesman does not need read-access to Roles table
	
	elsif user.salesmanager?
	
	can :read, Car
	can :read, Customer
#	can :read, Employee ---> salesmanager does not need read-access to Employee table
	can :read, Loan
	can :read, Quote
#	can :read, Role ---> salesmanager does not need read-access to Roles table
	
	elsif user.financemanager?
	
	can :read, Car
	can :read, Customer
	can :read, Employee
	can :manage, Loan
	can :manage, Quote
#	can :read, Role ---> financemanager does not need read-access to Roles table

# finance manage doesn't need access to roles
#  can :read, Role
		
	elsif user.inventorymanager?
	
	can :manage, Car
	can :read, Customer
	can :read, Employee
	can :read, Loan
	can :read, Quote
	can :read, Role
	
	else
	
# this means everybody else that does not have a role defined above can 
# pretty much only see the index.html.erb page of each table.
# everyone else can only see Cars table. 
	
	can :read, Car
#	can :read, Customer
#	can :read, Employee
#	can :read, Loan
#	can :read, Quote
#	can :read, Role	
	
	end
	
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
 end
end
