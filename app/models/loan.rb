class Loan < ApplicationRecord
  belongs_to :customer
  belongs_to :employee
  belongs_to :car

  def loan_amount_total
    self.loan_amount = (self.interest_rate * self.principal * self.year)
  end

end

