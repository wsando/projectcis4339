json.extract! quote, :id, :sales_person, :vin, :make, :model, :color, :sale_price, :employee_id, :car_id, :created_at, :updated_at
json.url quote_url(quote, format: :json)
