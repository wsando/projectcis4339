json.extract! customer, :id, :first_name, :last_name, :address, :city, :state, :zip_code, :phone_number, :email, :employee_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
