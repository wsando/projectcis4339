json.extract! loan, :id, :sales_person, :customer_first_name, :customer_last_name, :loan_amount, :principal, :interest_rate, :year, :make, :model, :color, :customer_id, :employee_id, :car_id, :created_at, :updated_at
json.url loan_url(loan, format: :json)
