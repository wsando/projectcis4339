json.extract! employee, :id, :employee_first_name, :employee_last_name, :email, :role_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
