class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  
end
class ApplicationController < ActionController::Base
  private

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
  URI.parse(request.referer).path if request.referer
end
end