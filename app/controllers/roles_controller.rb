class RolesController < ApplicationController
# commented out because line #4 takes care of this in Cancancan --->  before_action :set_car, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
	
	

  # GET /roles
  # GET /roles.json
  def index
   # @roles = Role.all
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
  end

  def descriptions
  #  @role = Role.find(params[:id])
    puts @role.role
    puts @role.description
    render json: @role
  end

  # GET /roles/new
  def new
  #  @role = Role.new
  end

  # GET /roles/1/edit
  def edit
  end

  # POST /roles
  # POST /roles.json
  def create
   # @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.html { redirect_to @role, notice: 'Role was successfully created.' }
        format.json { render :show, status: :created, location: @role }
      else
        format.html { render :new }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to @role, notice: 'Role was successfully updated.' }
        format.json { render :show, status: :ok, location: @role }
      else
        format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @role.destroy
    respond_to do |format|
      format.html { redirect_to roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   
    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:role, :description)
    end
end
