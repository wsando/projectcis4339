Rails.application.routes.draw do

 devise_for :users do
 
  get '/users/sign_out' => 'devise/sessions#destroy'
  get '/users/sign_in' => 'devise/sessions#new'

end

  resources :loans
  resources :customers
  resources :quotes
  resources :cars
  resources :employees
  
  get 'pdf_pages', :to => 'pdf_pages#show', format: 'pdf'

  resources :roles do
    member do
      get 'descriptions'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # comment
end
