class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.string :sales_person
      t.string :customer_first_name
      t.string :customer_last_name
      t.numeric :loan_amount
      t.numeric :principal
      t.numeric :interest_rate
      t.numeric :year
      t.string :make
      t.string :model
      t.string :color
      t.references :customer, foreign_key: true
      t.references :employee, foreign_key: true
      t.references :car, foreign_key: true

      t.timestamps
    end
  end
end
