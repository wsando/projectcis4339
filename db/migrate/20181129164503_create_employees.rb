class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :employee_first_name
      t.string :employee_last_name
      t.string :email
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
