class CreateQuotes < ActiveRecord::Migration[5.2]
  def change
    create_table :quotes do |t|
      t.string :sales_person
      t.string :vin
      t.string :make
      t.string :model
      t.string :color
      t.string :sale_price
      t.references :employee, foreign_key: true
      t.references :car, foreign_key: true

      t.timestamps
    end
  end
end
