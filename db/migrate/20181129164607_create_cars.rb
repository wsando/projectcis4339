class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :vin
      t.string :color
      t.string :model
      t.string :make
      t.string :wholesale_price

      t.timestamps
    end
  end
end
