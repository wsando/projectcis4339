# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

car = Car.create(vin: '1' , color: 'Blue', model: 'Toyota', make: '4Runner' , wholesale_price: '2000')
car = Car.create(vin: '2' , color: 'Red', model: 'Toyota', make: 'RAV4' , wholesale_price: '204650')
car = Car.create(vin: '3' , color: 'Green', model: 'Toyota', make: 'Sequoia' , wholesale_price: '20400')

role = Role.create(role: 'Sales Manager', description: 'Manages sales of car dealership. Can view all sales made by sales staff.')
role = Role.create(role: 'Salesman', description: 'Sells cars to customers, create customer quotes, can find cars in inventory.')
role = Role.create(role: 'Finance Manager', description: 'Manages car financing for customers. Can show customers breakdown of interest and principal.')
role = Role.create(role: 'Inventory Manager', description: 'Manages the car inventory of the dealership. Can upload images of vehicles to website.')
role = Role.create(role: 'Dealership Owner', description: 'Owns the dealership. Can perform any task in the system. Can also view total gross revenue, net profit and sales tax for dealership')
role = Role.create(role: 'General', description: 'Can view vehicles only in the dealership inventory.')

