require "application_system_test_case"

class LoansTest < ApplicationSystemTestCase
  setup do
    @loan = loans(:one)
  end

  test "visiting the index" do
    visit loans_url
    assert_selector "h1", text: "Loans"
  end

  test "creating a Loan" do
    visit loans_url
    click_on "New Loan"

    fill_in "Car", with: @loan.car_id
    fill_in "Color", with: @loan.color
    fill_in "Customer First Name", with: @loan.customer_first_name
    fill_in "Customer", with: @loan.customer_id
    fill_in "Customer Last Name", with: @loan.customer_last_name
    fill_in "Employee", with: @loan.employee_id
    fill_in "Interest Rate", with: @loan.interest_rate
    fill_in "Loan Amount", with: @loan.loan_amount
    fill_in "Make", with: @loan.make
    fill_in "Model", with: @loan.model
    fill_in "Principal", with: @loan.principal
    fill_in "Sales Person", with: @loan.sales_person
    fill_in "Year", with: @loan.year
    click_on "Create Loan"

    assert_text "Loan was successfully created"
    click_on "Back"
  end

  test "updating a Loan" do
    visit loans_url
    click_on "Edit", match: :first

    fill_in "Car", with: @loan.car_id
    fill_in "Color", with: @loan.color
    fill_in "Customer First Name", with: @loan.customer_first_name
    fill_in "Customer", with: @loan.customer_id
    fill_in "Customer Last Name", with: @loan.customer_last_name
    fill_in "Employee", with: @loan.employee_id
    fill_in "Interest Rate", with: @loan.interest_rate
    fill_in "Loan Amount", with: @loan.loan_amount
    fill_in "Make", with: @loan.make
    fill_in "Model", with: @loan.model
    fill_in "Principal", with: @loan.principal
    fill_in "Sales Person", with: @loan.sales_person
    fill_in "Year", with: @loan.year
    click_on "Update Loan"

    assert_text "Loan was successfully updated"
    click_on "Back"
  end

  test "destroying a Loan" do
    visit loans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Loan was successfully destroyed"
  end
end
